package id.co.nexsoft.country.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.country.model.District;
import id.co.nexsoft.country.repository.DistrictRepository;

import java.util.List;
import java.util.Optional;

@RestController
public class DistrictController {
    @Autowired
    private DistrictRepository repo;

    @GetMapping("/district")
    public List<District> getAllDistricts() {
        return repo.findAll();
    }

    @GetMapping("/district/{id}")
    public District getDistrictById(@PathVariable(value = "id") int id) {
        return repo.findById(id);
    }

    @PostMapping("/district")
    @ResponseStatus(HttpStatus.CREATED)
    public District createDistrict(@RequestBody District district) {
        return repo.save(district);
    }

    @DeleteMapping("/district/{id}")
    public void deleteDistrict(@PathVariable(value = "id") int id) {
        repo.deleteById(id);
    }

    @PutMapping("/district/{id}")
    public ResponseEntity<Object> updateDistrict(
            @RequestBody District district,
            @PathVariable int id) {

        Optional<District> districtRepo = Optional.ofNullable(repo.findById(id));

        if (!districtRepo.isPresent())
            return ResponseEntity.notFound().build();

        district.setId(id);
        repo.save(district);

        return ResponseEntity.noContent().build();
    }
}
