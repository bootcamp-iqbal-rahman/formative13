package id.co.nexsoft.country.service;

import java.util.List;

public class ProvinceCity {
    private String province;
    private List<String> city;

    public ProvinceCity(String province, List<String> city) {
        this.province = province;
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public List<String> getCity() {
        return city;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }
}
