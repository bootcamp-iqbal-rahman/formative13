package id.co.nexsoft.country.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.country.model.Country;

@Repository
public interface CountryRepository extends CrudRepository<Country, Integer> {
    Country findById(int id);
    List<Country> findAll();
    void deleteById(int id);
}