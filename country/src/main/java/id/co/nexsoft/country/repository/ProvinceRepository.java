package id.co.nexsoft.country.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.country.model.Province;

@Repository
public interface ProvinceRepository extends CrudRepository<Province, Integer> {
    Province findById(int id);
    List<Province> findAll();
    void deleteById(int id);
}