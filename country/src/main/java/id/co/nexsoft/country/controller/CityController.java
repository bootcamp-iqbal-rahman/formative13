package id.co.nexsoft.country.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.country.model.City;
import id.co.nexsoft.country.repository.CityRepository;

import java.util.List;
import java.util.Optional;

@RestController
public class CityController {
    @Autowired
    private CityRepository repo;

    @GetMapping("/city")
    public List<City> getAllCities() {
        return repo.findAll();
    }

    @GetMapping("/city/{id}")
    public City getCityById(@PathVariable(value = "id") int id) {
        return repo.findById(id);
    }

    @PostMapping("/city")
    @ResponseStatus(HttpStatus.CREATED)
    public City createCity(@RequestBody City city) {
        return repo.save(city);
    }

    @DeleteMapping("/city/{id}")
    public void deleteCity(@PathVariable(value = "id") int id) {
        repo.deleteById(id);
    }

    @PutMapping("/city/{id}")
    public ResponseEntity<Object> updateCity(
            @RequestBody City city,
            @PathVariable int id) {

        Optional<City> cityRepo = Optional.ofNullable(repo.findById(id));

        if (!cityRepo.isPresent())
            return ResponseEntity.notFound().build();

        city.setId(id);
        repo.save(city);

        return ResponseEntity.noContent().build();
    }
}
