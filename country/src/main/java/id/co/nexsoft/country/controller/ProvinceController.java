package id.co.nexsoft.country.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.country.model.City;
import id.co.nexsoft.country.model.Province;
import id.co.nexsoft.country.repository.ProvinceRepository;
import id.co.nexsoft.country.service.ProvinceCity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class ProvinceController {
    @Autowired
    private ProvinceRepository repo;

    @GetMapping("/province/{id}/all")
    public Province getAllProvince(@PathVariable(value = "id") int id) {
        return repo.findById(id);
    }

    @GetMapping("/province/{id}")
    public ResponseEntity<ProvinceCity> getProvinceAndCityById(@PathVariable int id) {
        Optional<Province> provinceOptional = Optional.ofNullable(repo.findById(id));

        return provinceOptional.map(province -> {
            List<String> city = province.getCities().stream()
                    .map(City::getCity)
                    .collect(Collectors.toList());

            ProvinceCity responseDTO = new ProvinceCity(province.getProvince(), city);

            return ResponseEntity.ok(responseDTO);
        }).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/province")
    @ResponseStatus(HttpStatus.CREATED)
    public Province createProvince(@RequestBody Province province) {
        return repo.save(province);
    }

    @DeleteMapping("/province/{id}")
    public void deleteProvince(@PathVariable(value = "id") int id) {
        repo.deleteById(id);
    }

    @PutMapping("/province/{id}")
    public ResponseEntity<Object> updateProvince(
            @RequestBody Province province,
            @PathVariable int id) {

        Optional<Province> provinceRepo = Optional.ofNullable(repo.findById(id));

        if (!provinceRepo.isPresent())
            return ResponseEntity.notFound().build();

        province.setId(id);
        repo.save(province);

        return ResponseEntity.noContent().build();
    }
}