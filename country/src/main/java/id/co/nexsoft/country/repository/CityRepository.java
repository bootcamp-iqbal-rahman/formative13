package id.co.nexsoft.country.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.country.model.City;

@Repository
public interface CityRepository extends CrudRepository<City, Integer> {  
    City findById(int id);
    List<City> findAll();
    void deleteById(int id);
}
