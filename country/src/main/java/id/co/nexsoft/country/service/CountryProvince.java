package id.co.nexsoft.country.service;

import java.util.List;

public class CountryProvince {
    private String country;
    private List<String> province;

    public CountryProvince() {}

    public CountryProvince(String country, List<String> province) {
        this.country = country;
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getProvince() {
        return province;
    }

    public void setProvince(List<String> province) {
        this.province = province;
    }
}
