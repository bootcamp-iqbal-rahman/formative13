package id.co.nexsoft.country.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.country.model.Country;
import id.co.nexsoft.country.model.Province;
import id.co.nexsoft.country.repository.CountryRepository;
import id.co.nexsoft.country.service.CountryProvince;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class CountryController {
    @Autowired
    private CountryRepository repo;

    @GetMapping("/country/{id}/all")
    public Country getAllCountry(@PathVariable(value = "id") int id) {
        return repo.findById(id);
    }

    @GetMapping("/country/{id}")
    public ResponseEntity<CountryProvince> getCountryAndProvinceById(@PathVariable int id) {
        Optional<Country> countryOptional = Optional.ofNullable(repo.findById(id));

        return countryOptional.map(country -> {
            List<String> province = country.getProvinces().stream()
                    .map(Province::getProvince)
                    .collect(Collectors.toList());

            CountryProvince responseDTO = new CountryProvince(country.getCountry(), province);

            return ResponseEntity.ok(responseDTO);
        }).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/country")
    @ResponseStatus(HttpStatus.CREATED)
    public Country createCountry(@RequestBody Country country) {
        return repo.save(country);
    }

    @DeleteMapping("/country/{id}")
    public void deleteCountry(@PathVariable(value = "id") int id) {
        repo.deleteById(id);
    }

    @PutMapping("/country/{id}")
    public ResponseEntity<Object> updateCountry(
            @RequestBody Country country,
            @PathVariable int id) {

        Optional<Country> countryRepo = Optional.ofNullable(repo.findById(id));

        if (!countryRepo.isPresent())
            return ResponseEntity.notFound().build();

        country.setId(id);
        repo.save(country);

        return ResponseEntity.noContent().build();
    }
}