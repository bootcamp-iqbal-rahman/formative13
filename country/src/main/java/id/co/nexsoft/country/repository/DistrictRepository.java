package id.co.nexsoft.country.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.country.model.District;

@Repository
public interface DistrictRepository extends CrudRepository<District, Integer> {
    District findById(int id);
    List<District> findAll();
    void deleteById(int id);
}
